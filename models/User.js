const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [ true, "First name is required"]
	},

	lastName:{
		type: String,
		required: [true, "Last name is required"]
	},

	email:{
		type: String,
		required: [true, "Email is required"]
	},

	isAdmin: {
		type: Boolean,
		default:true
	},

	mobileNo:{
		type: String,
		default: [true, "mobileNo is required"]
	},

	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "CourseId is required"]
			},

			enrolledOn: {
				type: Date,
				default: new Date()
			},

			status: {
				type: String,
				default: [true, "Enrolled"]
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);
